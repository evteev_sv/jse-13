package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.TaskService;

/**
 * Класс контроллера для связи задач/проектов
 * что бы не внедрять контроллеры задач и проектов между собой
 */
public class ProjectTaskController extends AbstractController {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final TaskController taskController = new TaskController(taskService);

    public ProjectTaskController() {
    }

    public ProjectController getProjectController() {
        return projectController;
    }

    public TaskController getTaskController() {
        return taskController;
    }

    /**
     * Просмотр проекта вместе с задачами
     *
     * @return код выполнения
     */
    public int viewProjectWithTasks() {
        System.out.println("[VIEW PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) System.out.println("Проект с таким ID не найден.");
        System.out.println(project);
        if (project.getTasks().size() > 0) {
            System.out.println("Задачи, назначенные на проект: ");
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(aLong);
                if (task != null) {
                    taskController.viewTask(task);
                }
            });
            System.out.println("[OK]");
        } else
            System.out.println("К данному проекту не привязана ни одна задача.");
        return 0;
    }

    /**
     * Добавление задачи к проекту
     *
     * @return задача, добавленная к проекту
     */
    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) return 0;
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) return 0;
        Task task = taskController.findTaskById(taskId);
        if (task == null) return 0;
        taskRepository.addTaskToProjectByIds(projectId, taskId);
        projectController.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление задачи из проекта по коду
     *
     * @return код исполнения
     */
    public int removeTaskFromProjectById() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        System.out.println("Введите код задачи:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        if (taskId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project == null) return 0;
        Task task = taskController.findTaskById(taskId);
        if (task == null) return 0;
        taskController.removeTaskFromProject(taskId);
        projectController.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка задач проекта
     *
     * @return код исполнения
     */
    public int clearProjectTasks() {
        System.out.println("[CLEAR PROJECT TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(projectId);
                if (task != null) {
                    taskController.removeTaskFromProject(aLong);
                }
            });
            project.getTasks().clear();
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление проекта вместе с задачами
     *
     * @return код исполнения
     */
    public int removeProjectWithTasks() {
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("Введите код проекта:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        Project project = projectController.findProjectById(projectId);
        if (project.getTasks().size() > 0) {
            project.getTasks().forEach(aLong -> {
                Task task = taskController.findTaskById(projectId);
                if (task != null) {
                    taskService.removeById(aLong);
                }
            });
            projectService.removeById(projectId);
        }
        System.out.println("[OK]");
        return 0;
    }

}
